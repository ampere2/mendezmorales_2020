# MendezMorales_BatteriesSupercaps_4_646_2021

Contains input files and data used to generate the figures of the article:

Computational Screening of the Physical Properties of Water-in-Salt Electrolytes

Trinidad Mendez-Morales, Zhujie Li and Mathieu Salanne,
*Batteries & Supercaps*, 4, 646-652, 2021

https://doi.org/10.26434/10.1002/batt.202000237 ([see here for a preprint](https://doi.org/10.26434/chemrxiv.13012646.v2))

*WiS-inputs.zip* contains typical [LAMMPS](https://lammps.sandia.gov/) input files for all the systems.

The folder *transport_coefficients* contains the computed viscosities, conductivities and diffusion coefficients for all the systems.

The folder *radial_distribution_functions* contains all the partial radial distribution functions for all the systems. The nomenclature of the atoms of the anion is provided in the file *figureS1.pdf* and the atoms from the water molecules are labelled Ow and Hw.

